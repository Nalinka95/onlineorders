package dao;

import bo.Order;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import connections.MongoConnection;
import org.apache.log4j.Logger;
import org.bson.Document;
import play.libs.Json;
import service.impl.OrderServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderDao {
    private static OrderDao orderDao;
    MongoDatabase mongoDatabase;
    MongoCollection ordersCollection;
    private static Logger logger ;

    public OrderDao() {
        logger = Logger.getLogger(OrderServiceImpl.class);
        mongoDatabase = MongoConnection.getMongoDatabase();
        ordersCollection = mongoDatabase.getCollection("orders");
    }

    public static OrderDao getInstance() {
        orderDao = orderDao == null ? new OrderDao() : orderDao;
        return orderDao;
    }

    public List<String> viewOrders() {
        List<String> orders = new ArrayList<>();
        MongoCursor<Document> objectsOfOrders = ordersCollection.find().iterator();
        while (objectsOfOrders.hasNext()) {
            orders.add(objectsOfOrders.next().toJson());
        }
        return orders;
    }

    public String addOrder(JsonNode requestedOrderData) {
        if (ordersCollection != null) {
            List<HashMap<String, String>> products = new ArrayList<>();
            Order insertingOrder = new Order((int) (ordersCollection.countDocuments() + 1), requestedOrderData.get("customerId").asInt(), requestedOrderData.get("date").asText(), requestedOrderData.get("orderStatus").asText());
            int numberOfProduct = requestedOrderData.get("products").size();
            for (int index = 0; index < numberOfProduct; index++) {
                String productId = requestedOrderData.get("products").get(index).get("productId").asText();
                String productQuantity = requestedOrderData.get("products").get(index).get("Quantity").asText();
                HashMap<String, String> product = new HashMap<>();
                product.put(productId, productQuantity);
                products.add(product);
            }
            insertingOrder.setProducts(products);
            JsonNode orderJson = Json.toJson(insertingOrder);
            Document orderDocument = Document.parse(orderJson.toString());
            ordersCollection.insertOne(orderDocument);
            logger.info("order was added successfully");
            return "Customer Id : " + insertingOrder.getCustomerId() + " :  successfully added..! ";
        } else {
            logger.info("adding order was failed.");
            return null;
        }

    }

    public JsonNode getOrderByCustomerId(JsonNode requestedOrderData) {
        int customerId = requestedOrderData.get("customerId").asInt();
        List<String> placedOrders = new ArrayList<>();
        MongoCursor<Document> objectsOfProducts = ordersCollection.find(new Document("customerId", customerId)).iterator();
        if (objectsOfProducts != null) {
            logger.info("object was found in getOderByCustomer");
            while (objectsOfProducts.hasNext()) {
                placedOrders.add(objectsOfProducts.next().toJson());
            }
        } else {
            logger.info("no placed order");
            return Json.toJson("no placed orders");
        }
        return Json.toJson(placedOrders);
    }

    public JsonNode changeOrderStatus(JsonNode requestedOrderData) {
        int orderId = requestedOrderData.get("orderId").asInt();
        String orderStatus = requestedOrderData.get("orderStatus").asText();
        ordersCollection.updateOne(Filters.eq("orderId", orderId), new Document("$set", new Document("orderStatus", orderStatus)));
        return Json.toJson("order status updated to " + orderStatus);

    }
    public String getOrderStatusById(int orderId){
        Document orderDocument= ((Document) ordersCollection.find(new Document("orderId", orderId)).first());
        String orderStatus=orderDocument.get("orderStatus").toString();
        return orderStatus;
    }

}
