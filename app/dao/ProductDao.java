package dao;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import connections.MongoConnection;
import org.bson.Document;

import play.libs.Json;

import java.util.ArrayList;
import java.util.List;

public class ProductDao {

    private static ProductDao productDao;
    MongoDatabase mongoDatabase;
    MongoCollection productsCollection;

    public ProductDao() {
        mongoDatabase = MongoConnection.getMongoDatabase();
        productsCollection=mongoDatabase.getCollection("products");
    }

    public static ProductDao getInstance(){
        productDao=productDao==null?new ProductDao():productDao;
        return productDao;
    }

    public List<String> getProducts() {
        List<String> products = new ArrayList<>();
        MongoCursor<Document> objectsOfProducts = productsCollection.find().iterator();
        while (objectsOfProducts.hasNext()) {
            products.add(objectsOfProducts.next().toJson());
        }
        return products;
    }
    public String addProduct(JsonNode requestedProductData){
        if(productsCollection!=null){
            Product insertingProduct=new Product(requestedProductData.get("ProductName").asText(),requestedProductData.get("stockQuantity").asInt(),requestedProductData.get("pricePerUnit").asDouble(),(int)productsCollection.countDocuments()+1);
            JsonNode productJson= Json.toJson(insertingProduct);
            Document productDocument= Document.parse(productJson.toString());
            productsCollection.insertOne(productDocument);
            return insertingProduct.getProductName()+" :  successfully added..! ";
        }
        else {
            return null;
        }
    }
    public JsonNode removeProductByName(JsonNode productData){
        String RemovingProductName=productData.get("ProductName").asText();
        productsCollection.deleteOne(Filters.eq("ProductName",RemovingProductName));
        return Json.toJson(RemovingProductName+" : deleted.");
    }
    public int getProductQuantityByProductId(int productId){
        Document productDocument= ((Document) productsCollection.find(new Document("productId", productId)).first());
        return Integer.parseInt(productDocument.get("productId").toString());
    }
}
