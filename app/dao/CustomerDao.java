package dao;

import bo.Customer;
import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import connections.MongoConnection;
import org.bson.Document;
import play.libs.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomerDao {
    private static CustomerDao customerDao;
    MongoDatabase mongoDatabase;
    MongoCollection customersCollection;

    public CustomerDao() {
        mongoDatabase = MongoConnection.getMongoDatabase();
        customersCollection=mongoDatabase.getCollection("customers");
    }

    public static CustomerDao getInstance(){
        customerDao=customerDao==null?new CustomerDao():customerDao;
        return customerDao;
    }

    public List<String> getCustomers() {
        List<String> customers = new ArrayList<>();
        MongoCursor<Document> customersObject = customersCollection.find().iterator();
        while (customersObject.hasNext()) {
            customers.add(customersObject.next().toJson());
        }
        return customers;
    }
    public String addCustomer(JsonNode requestedCustomerData){
        if(customersCollection!=null){
            HashMap<String,String> insertingAddress=new HashMap<String,String>();
            insertingAddress.put("addrline1",requestedCustomerData.get("address").get("addrline1").asText());
            insertingAddress.put("addrline2",requestedCustomerData.get("address").get("addrline2").asText());
            insertingAddress.put("streetNo",requestedCustomerData.get("address").get("streetNo").asText());
            insertingAddress.put("landmark",requestedCustomerData.get("address").get("landmark").asText());
            insertingAddress.put("city",requestedCustomerData.get("address").get("city").asText());
            insertingAddress.put("country",requestedCustomerData.get("address").get("country").asText());
            insertingAddress.put("zipcode",requestedCustomerData.get("address").get("zipcode").asText());
            Customer insertingCustomer=new Customer(requestedCustomerData.get("customerId").asInt(),requestedCustomerData.get("name").asText(),insertingAddress,requestedCustomerData.get("age").asText(),requestedCustomerData.get( "activeStatus").asBoolean());
            insertingCustomer.setCustomerId((int)customersCollection.countDocuments()+1);
            JsonNode customerJson= Json.toJson(insertingCustomer);
            Document customerDocument= Document.parse(customerJson.toString());
            customersCollection.insertOne(customerDocument);
            return insertingCustomer.getName()+" :  successfully added..! ";
        }
        else {
            return null;
        }
    }
    public JsonNode getCustomerById(int customerId){
        Document customerDocument= ((Document) customersCollection.find(new Document("customerId", customerId)).first());
        return Json.toJson(customerDocument);
    }
    public JsonNode isExistingCustomer(int customerId){
        Document customerDocument= ((Document) customersCollection.find(new Document("customerId", customerId)).first());
        return Json.toJson(customerDocument);
    }
    public JsonNode removeCustomerById(JsonNode customerData){
        int customerId=customerData.get("customerId").asInt();
        customersCollection.deleteOne(Filters.eq("customerId",customerId));
        return Json.toJson("deleted.");
    }
}
