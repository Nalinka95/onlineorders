package connections;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoConnection {
    private static MongoClient mongoClient;
    private static MongoDatabase mongoDatabase;


    public static MongoDatabase getMongoDatabase() {
        mongoClient = mongoClient == null ? new MongoClient() : mongoClient;
        mongoDatabase=mongoClient.getDatabase("zilingo");
        return mongoDatabase;
    }
}
