package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.CustomerServiceImpl;

public class CustomerController extends Controller {

    private CustomerServiceImpl customerServiceImpl;
    public CustomerController(){
        customerServiceImpl= new CustomerServiceImpl();
    }
    public Result viewAllCustomer() {
        return ok(String.valueOf(customerServiceImpl.viewAllCustomers()));
    }
    public Result addCustomer(Http.Request request){
        JsonNode requestedCustomerData= request.body().asJson();
        String response= customerServiceImpl.addCustomer(requestedCustomerData);
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
    public Result removeCustomerById(Http.Request request){
        JsonNode requestedCustomerData= request.body().asJson();
        String response= customerServiceImpl.removeCustomerById(requestedCustomerData).toString();
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
}
