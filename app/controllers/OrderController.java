package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.log4j.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.OrderServiceImpl;
import service.impl.ProductServiceImpl;

public class OrderController extends Controller {
    private OrderServiceImpl orderServiceImpl;
    private static Logger logger ;
    public OrderController(){
        logger = Logger.getLogger(OrderController.class);
        orderServiceImpl= new OrderServiceImpl();
        logger.debug("");
    }
    public Result viewAllOrders() {
        logger.info("view All order");
        logger.error("view All order");
        return ok(String.valueOf(orderServiceImpl.viewAllOrders()));
    }
    public Result addOrder(Http.Request request){
        JsonNode requestedOrderData= request.body().asJson();
        String response= orderServiceImpl.addOrder(requestedOrderData);
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
    public Result addOrderForValidCustomer(Http.Request request){
        JsonNode requestedOrderData= request.body().asJson();
        String response= orderServiceImpl.addOrderForValidCustomer(requestedOrderData);
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }

    public Result getOrderByCustomerId(Http.Request request){
        JsonNode requestedOrderDataByCustomerID= request.body().asJson();
        String response= String.valueOf(orderServiceImpl.getOrderByCustomerId(requestedOrderDataByCustomerID));
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
    public Result changeOrderStatus(Http.Request request){
        JsonNode orderUpdatingData= request.body().asJson();
        String response= String.valueOf(orderServiceImpl.changeOrderStatus(orderUpdatingData));
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }public Result updateStatusWithValidation(Http.Request request){
        JsonNode orderUpdatingData= request.body().asJson();
        String response= String.valueOf(orderServiceImpl.updateStatusWithValidation(orderUpdatingData));
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
}
