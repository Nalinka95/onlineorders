package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.ProductServiceImpl;

public class ProductController extends Controller {
    private ProductServiceImpl productServiceImpl;
    public ProductController(){
        productServiceImpl= new ProductServiceImpl();
    }
    public Result getAllProducts() {
        return ok(String.valueOf(productServiceImpl.getAllProducts()));
    }
    public Result addProduct(Http.Request request){
        JsonNode requestedProductData= request.body().asJson();
        String response= productServiceImpl.addProduct(requestedProductData);
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
    public Result removeProductByName(Http.Request request){
        JsonNode productData= request.body().asJson();
        String response= productServiceImpl.removeProductByName(productData).toString();
        if(response!=null){
            return ok(response);
        }
        else {
            return internalServerError("fail to connect to the database");
        }
    }
}
