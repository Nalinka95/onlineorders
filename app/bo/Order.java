package bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Order {
    private int orderId;
    private List<HashMap<String,String>> products;
    private int customerId;
    private String date;
    private String orderStatus;

    public Order(int orderId, List<HashMap<String, String>> products, int customerId, String date, String orderStatus) {
        setOrderId(orderId);
        setProducts(products);
        setCustomerId(customerId);
        setDate(date);
        setOrderStatus(orderStatus);

    }

    public Order(int orderId, int customerId, String date, String orderStatus) {
        setOrderId(orderId);
        setCustomerId(customerId);
        setDate(date);
        setOrderStatus(orderStatus);

        products= new ArrayList<>();
    }

    public int getOrderId() {
        return orderId;
    }

    private void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public List<HashMap<String, String>> getProducts() {
        return products;
    }

    public void setProducts(List<HashMap<String, String>> products) {
        this.products = products;
    }

    public int getCustomerId() {
        return customerId;
    }

    private void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getDate() {
        return date;
    }

    private void setDate(String date) {
        this.date = date;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
