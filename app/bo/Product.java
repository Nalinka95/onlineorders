package bo;

public class Product {
    private String productName;
    private int stockQuantity;
    private double pricePerUnit;
    private int productId;

    public Product(String productName, int stockQuantity, double pricePerUnit,int productId) {
        setProductName(productName);
        setStockQuantity(stockQuantity);
        setPricePerUnit(pricePerUnit);
        setProductId(productId);
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    private void setProductName(String productName) {
        this.productName = productName;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    private void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    private double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }
}
