package service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.ProductDao;
import org.apache.log4j.Logger;
import service.ProductService;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    private ProductDao productDao;
    private static Logger logger ;
    public ProductServiceImpl() {
        logger = Logger.getLogger(CustomerServiceImpl.class);
        productDao=ProductDao.getInstance();
    }
    public List<String> getAllProducts(){
        logger.info("get all products with the list");
        logger.error("get all products with the list");
        return productDao.getProducts();
    }

    public String addProduct(JsonNode requestedProductData){
        logger.info("add products");
        logger.error("add products");
        return productDao.addProduct(requestedProductData);
    }

    public JsonNode removeProductByName(JsonNode productData){
        logger.info("remove product by name");
        logger.error("remove product by name");
        return productDao.removeProductByName(productData);
    }
}
