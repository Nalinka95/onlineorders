package service.impl;

import bo.Order;
import com.fasterxml.jackson.databind.JsonNode;
import dao.CustomerDao;
import dao.OrderDao;
import dao.ProductDao;
import org.apache.log4j.Logger;
import play.libs.Json;
import service.OrderService;

import java.util.HashMap;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;
    private static Logger logger ;

    public OrderServiceImpl() {
        logger = Logger.getLogger(OrderServiceImpl.class);
        orderDao = OrderDao.getInstance();
    }

    public List<String> viewAllOrders() {
        logger.info("view All Orders");
        logger.error("view All Orders");
        return orderDao.viewOrders();
    }

    public String addOrder(JsonNode requestedOrderData) {
        logger.info("Add an Customer");
        logger.error("Add an Customer");
        return orderDao.addOrder(requestedOrderData);
    }

    public String addOrderForValidCustomer(JsonNode requestedOrderData) {
        logger.info("add order for valid Customer");
        logger.error("add order for valid Customer");
        int customerId = requestedOrderData.get("customerId").asInt();
        CustomerDao customerDao = CustomerDao.getInstance();
        JsonNode customerJson = null;
        if ((customerJson = customerDao.isExistingCustomer(customerId)) != null) {
            if (customerJson.get("activeStatus").asBoolean()) {
                if(isValidateOrderQuantity(requestedOrderData)){
                    return orderDao.addOrder(requestedOrderData);
                }
                else{
                    logger.info("stock is not enough for placed order");
                    return "There are not enough stock to place this order";
                }


            } else {
                logger.info("customer is not in ");
                return "customer is not in activate mode";
            }
        } else {
            logger.info("customer not found");
            return "customer not found.";
        }
    }

    public JsonNode getOrderByCustomerId(JsonNode requestedOrderData) {
        logger.info("get order by  Customer id");
        logger.error("get order by  Customer id");
        return orderDao.getOrderByCustomerId(requestedOrderData);
    }

    public JsonNode changeOrderStatus(JsonNode requestedOrderData) {
        logger.info("Change order status");
        logger.error("Change order status");
        return orderDao.changeOrderStatus(requestedOrderData);
    }

    public JsonNode updateStatusWithValidation(JsonNode requestedOrderData) {
        logger.info("update status with validation");
        logger.error("update status with validation");
        int orderId = requestedOrderData.get("orderId").asInt();
        String updatingStatus = requestedOrderData.get("orderStatus").asText();
        String previousStatus = orderDao.getOrderStatusById(orderId);
        if (sortStatus(previousStatus, updatingStatus)) {
            return orderDao.changeOrderStatus(requestedOrderData);
        } else {
            return Json.toJson("invalid active Status");
        }

    }

    public boolean sortStatus(String previousStatus, String currentStatus) {
        logger.info("sort the status with validation");
        logger.error("sort the status with validation");
        if (previousStatus.equalsIgnoreCase("Pending") && (currentStatus.equalsIgnoreCase("InProgress") || currentStatus.equalsIgnoreCase("cancelled"))) {
            return true;
        } else if (previousStatus.equalsIgnoreCase("InProgress") && (currentStatus.equalsIgnoreCase("Delivered") || currentStatus.equalsIgnoreCase("cancelled"))) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isValidateOrderQuantity(JsonNode requestedOrderData){
        logger.info("is valid order quantity method");
        logger.error("is valid order quantity method");
        int numberOfProduct = requestedOrderData.get("products").size();
        for (int index = 0; index < numberOfProduct; index++) {
            int productId = Integer.parseInt(requestedOrderData.get("products").get(index).get("productId").asText());
            int productQuantityOfOrder = Integer.parseInt(requestedOrderData.get("products").get(index).get("Quantity").asText());
            int existingProductQuantity= ProductDao.getInstance().getProductQuantityByProductId(productId);
            if(productQuantityOfOrder>existingProductQuantity){
                return false;
            }
        }
        return true;
    }

}
