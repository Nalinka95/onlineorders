package service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.CustomerDao;
import org.apache.log4j.Logger;
import service.CustomerService;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {
    private CustomerDao customerDao;
    private static Logger logger ;
    public CustomerServiceImpl() {
        logger = Logger.getLogger(CustomerServiceImpl.class);
        customerDao=CustomerDao.getInstance();
    }

    public List<String> viewAllCustomers(){
        logger.info("view All Customer");
        logger.error("view All Customer");
        return customerDao.getCustomers();
    }

    public String addCustomer(JsonNode requestedCustomerData){
        logger.info("Add a Customer");
        logger.error("Add a Customer");
        return customerDao.addCustomer(requestedCustomerData);
    }

    public JsonNode removeCustomerById(JsonNode customerData){
        logger.info("Remove a Customer");
        logger.error("Remove a Customer");
        return customerDao.removeCustomerById(customerData);
    }

}
